#@title Distance
#@date 22/10/2018
#@docauthor Luca Mayer-Dalverny
#@make

import Node
import math

#@fn d_euclidian
#@desc Calculate the euclidian distance between two Nodes (from Node.py)
#@param firstNode : the first Node
#@param secondNode : the second Node
#@return the distance between the two Nodes
#@bcode
def d_euclidian(firstNode, secondNode):
#@ecode
    
    distance = 0

    if firstNode.dimension == secondNode.dimension :
        for indexCoordinates in range (0,firstNode.dimension):
            distance = distance + math.pow((firstNode[indexCoordinates] - secondNode[indexCoordinates]),2)

        distance = math.sqrt(distance)

    else :
        distance = -1
        print("dimension error")
    
    return distance

def mainDistance():
    
    nodePremier = Node.Node(1024)
    nodeSecond = Node.Node(1024)

    nodePremier.fillCoordinatesWith(0)
    nodeSecond.fillCoordinatesWith(255)

    print("Node n°1 : ",nodePremier.coordinates)
    print("Node n°2 : ",nodeSecond.coordinates)

    nodePremier.distanceWithInput(nodeSecond,d_euclidian)

    print("Distance entre Node 1 et 2 : ",nodePremier.distance)
