#@title Node 
#@date 19/10/2018
#@docauthor Luca Mayer-Dalverny
#@make

import random
import matplotlib.image as mpimg
import numpy as np
import medianfilter
import Distance

#@class Node
#@desc Represent a node with a modular dimension to be used in a Self Organizing Map
class Node:

    #@sec Constructor

    #@method __init__
    #@desc The class constructor. Create a Node with a given dimension and fill it with 0.
    #@param dimension : the dimension of the node
    #@bcode
    def __init__(self,dimension):
    #@ecode
        self.coordinates = []
        self.__dimension = dimension
        self.label = "0"
        self.__distanceFromInput = -1
        for indexCoordinate in range (0,self.__dimension):
            self.coordinates.append(0)

    #@sec Filling coordinates methods

    #@method fillCoordinatesWith
    #@desc Fill the coordinates of the node with a given value.
    #@param value : the value
    #@bcode
    def fillCoordinatesWith(self,value):
    #@ecode
        for indexCoordinate in range (0,self.__dimension):
            self[indexCoordinate] = value

    #@method fillCoordinatesAtRandom
    #@desc Fill the coordinates of the node with random value between a given minimum and a given maximum.
    #@param minValue : the minimum value for a coordinate
    #@param maxValue : the maximum value for a coordinate
    #@bcode
    def fillCoordinatesAtRandom(self,minValue,maxValue):
    #@ecode
        for indexCoordinate in range (0,self.__dimension):
            self[indexCoordinate] = random.randint(minValue,maxValue)

    #@sec Relation with other Nodes methods

    #@method distanceWithInput
    #@desc Calculate the distance between this Node and another Node, following the distance function given.
    #@param inputNode : the node to calculate the distance with
    #@param distanceFunction : the function that calculate the distance between two Nodes (Use the functions in Distance.py)
    #@return the distance between the two Nodes
    #@bcode
    def distanceWithInput(self,inputNode,distanceFunction):
    #@ecode
        self.__distanceFromInput = distanceFunction(self,inputNode)

    #@method learn
    #@desc Recalculate the coordinates to be closer to another Node, depending on the learning speed.
    #@param inputNode : the node to learn on
    #@param learningSpeed : the learning speed (between -1 and 1, if not it wiil be put to 0)
    #@bcode
    def learn(self,inputNode,learningSpeed):
    #@ecode
        if learningSpeed < -1 or learningSpeed > 1 :
            learningSpeed = 0
        
        if self.dimension == inputNode.dimension :
            for indexCoordinates in range (0,self.dimension):
                self[indexCoordinates] += (learningSpeed * (inputNode[indexCoordinates] - self[indexCoordinates]))
        else :
            print("dimension error")

    #@method copyFrom
    #@desc Copy the value of another Node into this node (if the dimension of the two Nodes are equals).
    #@param otherNode : the node to copy
    #@bcode
    def copyFrom(self,otherNode):
    #@ecode
        if self.dimension == otherNode.dimension :
            for indexCoordinates in range (0,self.__dimension):
                self[indexCoordinates] = otherNode[indexCoordinate]

    #@sec Getters and setters

    #@method dimension
    #@desc Give the dimension of the Node.
    #@return (int) the dimension of the Node
    @property
    #@bcode
    def dimension(self):
    #@ecode
        return self.__dimension

    #@method distance
    #@desc Give the distance previously calculated between the node and the Input of the Self Organizing Map
    #@return (float) the distance (-1 if the method distanceWithInput() has never been called)
    @property
    #@bcode
    def distance(self):
    #@ecode
        return self.__distanceFromInput
    
    #@method __getitem__
    #@desc Give the coordinate at a given index.
    #@param index : the index where the coordinate wanted is
    #@return the coordinate wanted
    #@bcode
    def __getitem__(self,index):
    #@ecode
        returnValue = 0
        if index >= 0 and index < self.__dimension :
            returnValue = self.coordinates[index]
        return returnValue

    #@method __setitem__
    #@desc set a value to the coordinate at a given index.
    #@param index : the index where the coordinate to modify is
    #@param value : the new value of the coordinate
    #@bcode
    def __setitem__(self,index,value):
    #@ecode
        self.coordinates[index] = value

    #@method createFromImage
    #@desc create a Node from a grey level image
    #@param Image : the image from wich we create the node
    #@bcode
    def createFromImage(self,Image):
    #@ecode
        currentIndex = 0
        width = Image.shape[0]
        height = Image.shape[1]
        for abscissaCoord in range(0,width) :
            for ordinateCoord in range(0,height) :
                value = Image[abscissaCoord,ordinateCoord,0]
                self[currentIndex]=value * 255
                currentIndex += 1
                


#@chap Main program

#In the following example, we create a Node of dimension 100, fill it with the -1 value and then fill it with random value between 0 and 255.

def mainNode():
    
#@bcode
    '''
    node = Node(100)
    print(node.label)
    node.label = "Hello"
    print(node.label)
    print("Node initialized :\n")
    print(node.coordinates)
    print("\nNode filled with -1 : \n")
    node.fillCoordinatesWith(-1)
    print(node.coordinates)
    print("\nNode filled with random values between 0 and 255 : \n")
    node.fillCoordinatesAtRandom(0,255)
    print(node.coordinates)
    '''

    imageToAnalyse = mpimg.imread("./data/a/a-1.png")
    #greyImageToAnalyse = medianfilter.rgb2grey(imageToAnalyse)
    #medianImageToAnalyse = medianfilter.medianfilter(greyImageToAnalyse)
    nodeImageToAnalyse = Node(imageToAnalyse.shape[0]*imageToAnalyse.shape[1])
    nodeImageToAnalyse.createFromImage(imageToAnalyse)

    imageToAnalyse2 = mpimg.imread("./data/a/a-5.png")
    #greyImageToAnalyse2 = medianfilter.rgb2grey(imageToAnalyse2)
    #medianImageToAnalyse2 = medianfilter.medianfilter(greyImageToAnalyse2)
    nodeImageToAnalyse2 = Node(imageToAnalyse2.shape[0]*imageToAnalyse2.shape[1])
    nodeImageToAnalyse2.createFromImage(imageToAnalyse2)

    nodeImageToAnalyse.distanceWithInput(nodeImageToAnalyse2,Distance.d_euclidian)

    print(nodeImageToAnalyse.distance)

#@ecode
