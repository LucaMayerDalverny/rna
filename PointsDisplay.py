import matplotlib.pyplot as plt
import numpy as np
import Node
import SOM
from random import randrange

#From https://gist.github.com/jakevdp/91077b0cae40f8f8244a
def discrete_cmap(N, base_cmap=None):
    """Create an N-bin discrete colormap from the specified input map"""

    # Note that if base_cmap is a string or None, you can simply do
    #    return plt.cm.get_cmap(base_cmap, N)
    # The following works for string, None, or a colormap instance:

    base = plt.cm.get_cmap(base_cmap)
    color_list = base(np.linspace(0, 1, N))
    cmap_name = base.name + str(N)
    return base.from_list(cmap_name, color_list, N)

    

def display(PointSOM,Dim1ToDisplay,Dim2ToDisplay):
    #Definition of three list of coordonates 
    x = []
    y = []
    c = []

    for Point in PointSOM.nodesList :
        #The abscissa axis took the value od the coordinates of the Dim1ToDisplay coordinate
        x.append(Point.coordinates[Dim1ToDisplay])
        #Same for the ordinate with the Dim2ToDisplay
        y.append(Point.coordinates[Dim2ToDisplay])

        c.append(ord(Point.label))

    #Creation of the figure
    fig,graph1 = plt.subplots(figsize=(7,7))

    #Création of the graphical view
    sctr = graph1.scatter(x=x, y=y, marker='o', c=c, cmap=discrete_cmap(26,'gist_ncar'))
    #display of the colorbar
    cbar = plt.colorbar(sctr, ticks=alNum)
    cbar.ax.set_yticklabels(alphabet)

    graph1.set_title("Graphique de représentation 2D")
    graph1.set_xlabel('$x$')
    graph1.set_ylabel('$y$')

def TotalDisplay(PointSOM,Dim1ToDisplay,Dim2ToDisplay):
    #Verification that the dimension ask to be display exist in the Point
    if Dim1ToDisplay>=PointSOM.nodesDimension or Dim1ToDisplay<0 or Dim2ToDisplay<0 or Dim2ToDisplay>=PointSOM.nodesDimension :
        #Print an error message
        print("\n********************************************\nProbleme avec les dimensions à afficher\n********************************************")
        #Quit the program
        raise SystemExit
    plt.ioff()
    display(PointSOM,Dim1ToDisplay,Dim2ToDisplay)
    plt.show()


alphabet=['a','z','e','r','t','y','u','i','o','p','q','s','d','f','g','h','j','k','l','m','w','x','c','v','b','n']
alphabet.sort()
alNum=[]
for letter in alphabet :
    alNum.append(ord(letter))


"""
Test des fonctions avec des points au hasard
"""

def mainDisplay():

    mySOM = SOM.SOM(1000,5)
    mySOM.far(0,255)
    for a in mySOM.nodesList :
        myRand = randrange(len(alphabet))
        a.label=alphabet[myRand]

        
    TotalDisplay(mySOM,4,1)
