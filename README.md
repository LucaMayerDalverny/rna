# Réseau de Neurones Artificiels

Réseau de Neurone Artificiel (ou RNA) est un mini projet de reconnaissance de lettres réalisé dans le cadre du cours de réseau neuronal de M1 de l'ISEN Toulon.

## Sommaire

1. [Informations utiles](#info)
    1. [Classes conseillées](#classes)
    2. [Concepts requis](#concepts)
2. [À propos](#apropos)
    1. [Comment ça marche ?](#comment)
    2. [Pour qui est-ce que cela a été conçu ?](#qui)
3. [Utilisation dans vos projets](#utilisation)
    1. [Pré-requis](#prerequis)
    2. [Installation](#installation)
    3. [Utilisation](#utilisation)
4. [Crédits](#credits)
5. [Alternatives](#alternatives)
6. [Mainteneurs](#mainteneurs)

## Informations utiles <a name=info></a>

### Classes conseillées <a name=classes></a>

+ Année minimum : M1
+ Domaine Professionnel : Tous
+ Cycle fait : Tous

### Concepts requis <a name=concepts></a>

+ Maitrise du language de programmation [Python](https://www.python.org/)
+ Connaissance des [cartes de Kohonen](https://fr.wikipedia.org/wiki/Carte_auto_adaptative)

## À propos de RNA <a name=apropos></a>

### Comment ça marche ? <a name=comment></a>

Le programme est écrit intégralement en Python et utilise les cartes auto-adaptative pour reconnaître des caractère manuscrits.

### Pour qui est-ce que cela a été conçu ? <a name=qui></a>

## Utilisation de RNA dans vos projets <a name=utilisation></a>

### Pré-requis <a name=prerequis></a>

[Python](https://www.python.org/) doit être installé sur votre machine pour faire tourner le programme. Matplotlib doit également être installé. son installation se fait comme suit, dans un invite de commande :

```bash
pip install matplotlib --user
```


### Installation <a name=installation></a>

Dans le dossier où vous voulez mettre le projet, ouvrez un invite de commande et tapez les commandes suivantes :

```bash
git clone "https://framagit.org/LucaMayerDalverny/rna.git"
cd rna
```

### Utilisation <a name=utilisation></a>

[Mettre un lien vers le manuel d'utilisation ou, s'il n'y en a pas, mettre les instructions d'utilisation]

## Crédits <a name=credits></a>

+ [Matplotlib](https://matplotlib.org/)

## Alternatives <a name=alternatives></a>

## Mainteneurs <a name=mainteneurs></a>

[@iamClement](https://framagit.org/iamClement)  
[@LucaMayerDalverny](https://framagit.org/LucaMayerDalverny)  
[@AnneS](https://framagit.org/AnneS)  