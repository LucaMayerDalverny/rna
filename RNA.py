import os
import SOM
import Node
import medianfilter
import PointsDisplay
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

def learnFromData(som,listFiles,label,path):

    #Pour chaque image dans la liste de fichier
    for indexImage in range (0,len(listFiles)):
        #Ouverture de l'image
        image = mpimg.imread(path+"/"+listFiles[indexImage])
        #Niveau de gris
        #greyImage = medianfilter.rgb2grey(image)
        #Filtre médian
        #medianImage = medianfilter.medianfilter(greyImage)
        #Creation du Node
        nodeImage = Node.Node(image.shape[0]*image.shape[1])
        #Initialisation du Node avec les valeur de l'image
        nodeImage.createFromImage(image)
        #Ajout du label au Node
        nodeImage.label = label

        #plt.imshow(image)
        #plt.show()

        #print(nodeImage.coordinates[10])
        #Apprentissage de la 'SOM' avec le nodeImage
        som.learn(nodeImage,250,250,125)
    print(label,"learned")

print("Start learning")

som = SOM.SOM(10000,24*24)

som.far(0,255)

A_files = os.listdir("./data/a")
E_files = os.listdir("./data/e")
#H_files = os.listdir("./data-thin/h")
I_files = os.listdir("./data/i")
N_files = os.listdir("./data/n")
T_files = os.listdir("./data/t")
V_files = os.listdir("./data/v")
W_files = os.listdir("./data/w")
X_files = os.listdir("./data/x")
Z_files = os.listdir("./data/z")
#Z_files = os.listdir("./Letters2/Z")

##################
# Images de test #
##################

#Création du A de test
A = mpimg.imread("./data/a/a-2.png")
nodeA = Node.Node(A.shape[0]*A.shape[1])
nodeA.createFromImage(A)

#Création du E de test
E = mpimg.imread("./data/e/e-7.png")
nodeE = Node.Node(E.shape[0]*E.shape[1])
nodeE.createFromImage(E)

#Création du I de test
I = mpimg.imread("./data/i/i-9.png")
nodeI = Node.Node(I.shape[0]*I.shape[1])
nodeI.createFromImage(I)

#Création du N de test
N = mpimg.imread("./data/n/n-5.png")
nodeN = Node.Node(N.shape[0]*N.shape[1])
nodeN.createFromImage(N)

#Création du T de test
T = mpimg.imread("./data/t/t-3.png")
nodeT = Node.Node(T.shape[0]*T.shape[1])
nodeT.createFromImage(T)

#Création du V de test
V = mpimg.imread("./data/v/v-2.png")
nodeV = Node.Node(V.shape[0]*V.shape[1])
nodeV.createFromImage(V)

#Création du W de test
W = mpimg.imread("./data/w/w-4.png")
nodeW = Node.Node(W.shape[0]*W.shape[1])
nodeW.createFromImage(W)

#Création du Z de test
X = mpimg.imread("./data/x/x-5.png")
nodeX = Node.Node(X.shape[0]*X.shape[1])
nodeX.createFromImage(X)

#Création du Z de test
Z = mpimg.imread("./data/z/z-1.png")
nodeZ = Node.Node(Z.shape[0]*Z.shape[1])
nodeZ.createFromImage(Z)

#################
# Apprentissage #
#################

learnFromData(som,A_files,"A","./data/a")
#som.identify(nodeImageToAnalyse,5000)
learnFromData(som,E_files,"E","./data/e")
#som.identify(nodeImageToAnalyse,500)
#learnFromData(som,H_files,"H","./data-thin/h")
#som.identify(nodeImageToAnalyse,500)
learnFromData(som,I_files,"I","./data/i")
#som.identify(nodeImageToAnalyse,5000)
learnFromData(som,N_files,"N","./data/n")
#som.identify(nodeImageToAnalyse,500)
learnFromData(som,T_files,"T","./data/t")
learnFromData(som,V_files,"V","./data/v")
learnFromData(som,W_files,"W","./data/w")
learnFromData(som,X_files,"X","./data/x")
learnFromData(som,Z_files,"Z","./data/z")

#############
# Résultats #
#############

#print("Resultat de l'analyse : ")
#print("Résultat pour le A :")
#som.identify(nodeA,20)
#print("Résultat pour le E :")
#som.identify(nodeE,20)
#print("Résultat pour le I :")
#som.identify(nodeI,20)
#print("Résultat pour le N :")
#som.identify(nodeN,20)
#print("Résultat pour le T :")
#som.identify(nodeT,20)
#print("Résultat pour le V :")
#som.identify(nodeV,20)
#print("Résultat pour le W :")
#som.identify(nodeW,20)
#print("Résultat pour le X :")
#som.identify(nodeX,20)
#print("Résultat pour le Z :")
#som.identify(nodeZ,20)
print("Contenu de la carte : ")
som.identify(nodeI,10000)

PointsDisplay.TotalDisplay(som,0,1)

########
# menu #
########

choice = 1

while( choice != 0):

    print("===============RNA===============")
    print("= 0 : quitter                   =")
    print("= 1 : reconnaissance de lettre  =")
    print("=================================")
    choice = int(input("Entrez un choix : "))

    if (choice == 1) :

        print("===========Reconnaissance========")
        
        filename = input("entrez le nom du fichier : ")

        image = mpimg.imread("./data/0 analyse/"+filename+".png")

        nodeImage = Node.Node(image.shape[0]*image.shape[1])

        nodeImage.createFromImage(image)

        print("=========Resultats===============")

        som.identify(nodeImage,20)
    


