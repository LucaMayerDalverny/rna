#@title Result
#@date 15/11/2018
#@docauthor Luca Mayer-Dalverny
#@make

#@class Result
#@desc Represent a result defined by its value and its number of occurrence
class Result:

    #@method __init__
    #@desc Create a result object
    #@param value : the value of the result
    def __init__(self,value):
        self.__value = value
        self.occurrence = 1

    #@method found
    #@desc Increment the number of occurrence of the result
    #@bcode
    def found(self):
    #@ecode
        self.occurrence += 1

    #@method value
    #@desc Give the value of the result
    #@return the value of the result
    @property
    #@bcode
    def value(self):
    #@ecode
        return self.__value
