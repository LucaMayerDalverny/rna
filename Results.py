#@title Results
#@date 15/11/2018
#@docauthor Luca Mayer-Dalverny
#@make

import Result

#@class Results
#@desc Respresent a list of results
class Results:

    def __init__(self):
        self.__sampleSize = 0
        self.__resultNumber = 0
        self.__resultList = []

    #@method valueFound
    #@desc Add a value in the result list by creating a new result or incrementing the number of occurence of a result previously found
    #@param value : the value found
    #@bcode
    def valueFound(self,value):
    #@ecode
        self.__sampleSize +=1

        indexFound = -1

        for indexResult in range(0,self.__resultNumber):
            if value == self.__resultList[indexResult].value :
                indexFound = indexResult

        if indexFound == -1 :
            self.__resultList.append(Result.Result(value))
            self.__resultNumber +=1 

        else :
            self.__resultList[indexFound].found()

    #@method displayResults
    #@desc Display the results ordered by the number of occurrence
    #@param resultInPercent : display the result in percent (1) or in occurrence number (0)
    #@bcode
    def displayResults(self,resultInPercent):
    #@ecode
        divider = self.__sampleSize
        multiplier = 100
        unity = "%"
        
        if resultInPercent == 0:
            divider = 1
            multiplier = 1
            unity = "occurrence"

        self.__resultList.sort(key = lambda result : result.value)

        for indexResult in range(0,self.__resultNumber):
            print(self.__resultList[indexResult].value,":",multiplier*((self.__resultList[indexResult].occurrence)/divider),unity)

def mainResults():

    results = Results()

    results.valueFound("a")
    results.valueFound("b")
    results.valueFound("a")
    results.valueFound("w")
    results.valueFound("w")
    results.valueFound("a")

    results.displayResults(0)
