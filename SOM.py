#@title SOM (Self Organizing Map)
#@date 19/10/2018
#@docauthor Luca Mayer-Dalverny, Clement Gicquel
#@make

import Distance
import Node
import Results

#@class SOM (Self Organizing Map)
#@desc Represent a kohonen map
class SOM:

    #@sec Constructor

    #@method __init__
    #@author Luca Mayer-Dalverny
    #@desc The class constructor
    #@param nodeNumber : The number of nodes in the map
    def __init__(self,nodesNumber,nodesDimension):
        self.__nodesNumber = nodesNumber
        self.__nodesDimension = nodesDimension
        self.nodesList = []
        
        for indexNode in range (0,self.__nodesNumber):
            self.nodesList.append(Node.Node(nodesDimension))

    #@sec Processing Methods

    #@method far (fill at random)
    #@author Luca Mayer-Dalverny
    #@desc Fill the coordinates of the Nodes in the Self Organizing Map at random between two given values
    #@param minValue : the minimum value for a coordinate
    #@param maxValue : the maximum value for a coordinate
    #@bcode
    def far(self,minValue,maxValue):
    #@ecode
        for indexNode in range (0,self.__nodesNumber):
            self[indexNode].fillCoordinatesAtRandom(minValue,maxValue)

    #@method learn
    #@author Clement Gicquel, Luca Mayer-Dalverny
    #@desc Made the self organizing map learn on a given Node.
    #@param nodeInput : the Node on with the map will learn
    #@param vcn (very close neighbour) : the number of very close neighbour, who will have a learning speed factor of 0.75
    #@param cn (close neighbour) : the number of close neighbour, who will have a learning speed factor of 0.30
    #@param n (neighbour) : the number of neighbour, who will have a learning speed factor of -0.25
    #@bcode
    def learn(self,nodeInput,vcn,cn,n):
    #@ecode
        if vcn+cn+n+1 < self.nodesNumber :
            for indexNode in range (0,self.__nodesNumber):
                self[indexNode].distanceWithInput(nodeInput, Distance.d_euclidian)

            self.nodesList.sort(key = lambda node : node.distance)

            #The closer node learn with a learning speed of 1
            self[0].learn(nodeInput,1)
            #And will have the same label as the input Node
            self[0].label = nodeInput.label

            #The very close neighbours will learn with a learning speed of 0.75
            for indexVeryClose in range (1,1+vcn):
                self[indexVeryClose].learn(nodeInput,0.75)
                #And will have the same label as the input Node
                self[indexVeryClose].label = nodeInput.label

            #The close neighbours will learn with a learning speed of 0.30
            for indexClose in range (1+1+vcn,1+1+vcn+cn):
                self[indexClose].learn(nodeInput,0.30)
                #And will have the same label as the input Node
                self[indexClose].label = nodeInput.label

            #The close neighbours will learn with a learning speed of -0.25
            for indexNeighbour in range(1+1+1+vcn+cn,1+1+1+vcn+cn+n):
                self[indexNeighbour].learn(nodeInput,-0.25)

    #@method identify
    #@desc Identify the Node given in input thanks to the label
    #@author Luca Mayer-Dalverny
    #@param nodeInput : the Node to identify
    #@param neighbourNumber : the size of the Node sample to analyse the input Node
    #@bcode
    def identify(self,nodeInput,neighbourNumber):
    #@ecode
        if neighbourNumber <= self.nodesNumber :
            for indexNode in range (0,self.__nodesNumber):
                self[indexNode].distanceWithInput(nodeInput, Distance.d_euclidian)

            self.nodesList.sort(key = lambda node : node.distance)

            results = Results.Results()

            for indexNeighbour in range (0,neighbourNumber):
                results.valueFound(self[indexNeighbour].label)

            results.displayResults(1)
                

    #@sec Getters and setters

    #@method nodesNumber
    #@author Luca Mayer-Dalverny
    #@desc Give the number of Nodes in the Self Organizing Map
    #@return (int) the number of Nodes
    @property
    #@bcode
    def nodesNumber(self):
    #@ecode
        return self.__nodesNumber

    #@method nodesDimension
    #@author Luca Mayer-Dalverny
    #@desc Give the dimension of the Nodes in the Self Organizing Map
    #@return (int) the dimension of the Nodes
    @property
    #@bcode
    def nodesDimension(self):
    #@ecode
        return self.__nodesDimension

    #@method __getitem__
    #@author Luca Mayer-Dalverny
    #@desc Give the Node of the Self Organizing Map at a given index
    #@param index : the index of the Node wanted
    #@return (Node) the Node wanted
    #@bcode
    def __getitem__(self,index):
    #@ecode
        returnValue = 0
        if index >= 0 and index < self.__nodesNumber :
            returnValue = self.nodesList[index]
        return returnValue

#@chap Main program

#In the following example : . 
#@bitem
#@item We create a Self Organizing Map (SOM) with 5 nodes with a dimension of 2.
#@item We fill the Nodes coordinates at random with values between 0 and 255 and we display the Nodes coordinates.
#@item We create a 'reference Node' of dimension 2 with random value and we display it.
#@item With the call to the method 'learn', we calculate the distance between the Nodes in the SOM and the 'reference Node' and we sort these Nodes by the distance we previously calculated.
#@eitem

#@bcode

def mainSOM():
    som = SOM(10,25)
    som.far(0,255)

    #for i in range(0,som.nodesNumber):
    #    print("Node n°",i," : ",som[i].coordinates)

    nodeI = Node.Node(25)
    nodeI[7] = 255
    nodeI[17] = 255
    nodeI[22] = 255
    nodeI.label = "I"

    #print("Node Input : ",nodeRef.coordinates)

    som.learn(nodeI,1,1,2)

    #for i in range(0,som.nodesNumber):
    #    print("Node n°",i," : ",som[i].coordinates,"avec la distance : ",som[i].distance)

    nodeW = Node.Node(25)
    nodeW[5] = 210
    nodeW[7] = 210
    nodeW[9] = 210
    nodeW[10] = 255
    nodeW[12] = 255
    nodeW[14] = 255
    nodeW[15] = 255
    nodeW[17] = 255
    nodeW[19] = 255
    nodeW[22] = 255
    nodeW[20] = 190
    nodeW[21] = 190
    nodeW[23] = 190
    nodeW[24] = 190
    nodeW.label = "W"

    nodeWW = Node.Node(25)
    nodeWW[5] = 210
    nodeWW[7] = 210
    nodeWW[9] = 210
    nodeWW[10] = 255
    nodeWW[12] = 255
    nodeWW[14] = 255
    nodeWW[15] = 255
    nodeWW[17] = 255
    nodeWW[19] = 255
    nodeWW[22] = 255
    nodeWW[20] = 0
    nodeWW[21] = 190
    nodeWW[23] = 190
    nodeWW[24] = 0
    nodeWW.label = "W"

    #print("Second Node Input : ",nodeRefDeux.coordinates)

    som.learn(nodeWW,1,1,2)
    som.learn(nodeW,1,1,2)

    #for i in range(0,som.nodesNumber):
    #    print("Node n°",i," : ",som[i].coordinates,"avec la distance : ",som[i].distance)

    nodeII = Node.Node(25)
    nodeII[2] = 255
    nodeII[12] = 255
    nodeII[17] = 255
    nodeII[22] = 255
    nodeII.label = "I"

    som.learn(nodeII,1,1,2)

    nodeIII = Node.Node(25)
    nodeIII[2] = 255
    nodeIII[12] = 190
    nodeIII[7] = 50
    nodeIII[17] = 255
    nodeIII[22] = 255   

    som.identify(nodeIII,10)

#@ecode
