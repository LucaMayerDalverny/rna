import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

'''
Description:
===============================================
median filter of a grayscale image
===============================================
'''

def rgb2grey(rgb):
    greyscale = np.copy(rgb) # On fait une copie de l'original
    for i in range(greyscale.shape[0]):
        for j in range(greyscale.shape[1]):

            r = greyscale[i, j, 0]
            g  = greyscale[i, j, 1]
            b = greyscale[i, j, 2]

            grey = 0.299*r+0.587*g+0.114*b
            greyscale[i, j] = [grey, grey, grey, greyscale[i,j,3]]

    return greyscale

def medianfilter(greyscale):
    blurredimg = np.copy(greyscale)
    n_pixel = np.zeros((9))

    for i in range(greyscale.shape[0]-1):
        for j in range(greyscale.shape[1]-1):
            if j > 0 and i > 0:
                n_pixel[0] = greyscale[i-1,j-1,0]
                n_pixel[1] = greyscale[i-1,j,0]
                n_pixel[2] = greyscale[i-1,j+1,0]
                n_pixel[3] = greyscale[i,j-1,0]
                n_pixel[4] = greyscale[i,j,0]
                n_pixel[5] = greyscale[i,j+1,0]
                n_pixel[6] = greyscale[i+1,j-1,0]
                n_pixel[7] = greyscale[i+1,j,0]
                n_pixel[8] = greyscale[i+1,j+1,0]
                medianpixel = np.sort(n_pixel)  
                blurredimg[i,j] = [medianpixel[4], medianpixel[4], medianpixel[4], greyscale[i,j,3]]
    return blurredimg

def mainMedianFilter():
    rgb = mpimg.imread('e2_direct.bmp')
    greyscale = rgb2grey(rgb)

    blurredimg = medianfilter(greyscale)
    plt.imshow(blurredimg)
    plt.show()
